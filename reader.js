/**
 * https://github.com/sortasc/reader.js
 * Depends https://developers.google.com/feed/v1/devguide
 * Usage: $('#feed').reader('http://foster.sorta.in/rss');
 */

$(function() {

  $.fn.reader = function(rss) {
    var feed = new google.feeds.Feed(rss)
      , self = this;

    feed.setNumEntries(10);
    
    feed.load(function(result) {
      if (!result.error) {
        $.each(result.feed.entries, function(item){
          var entry = result.feed.entries[item];

          $(self).link_to(entry.title, entry.link);

        });
      }
    });
    async(self);
  },

  // create a link tag element
  $.fn.link_to = function(title, link){
    var el = $('<a />')
        .attr('href', link)
        .html(title)
        .wrap('<p />')
        .parent();
    $(this).append(el);
  },

  // up/down elements of array
  $.fn.cover = function(el){
    var self = this
      , elements = $(self).find(el)
      , first = elements.first();

    $(first).slideUp('slow', function(){
      var that = this;
      $(first).remove();
      $(self).append($(that).show());
    });

  }
});


function async(el){
  setTimeout(function scrolling(){
    $(el).cover('p');
    setTimeout(scrolling, 2700);
  }, 2700);
}
